<?php
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles', '11');
function my_theme_enqueue_styles() {
	$parent_style = 'parent-style';
	
	// Load jQuery Webticker
	wp_enqueue_style('jquery-webticker-style', get_stylesheet_directory_uri() . '/inc/webticker/jquery.webticker.css');

	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
	
	// Load jQuery Webticker
	wp_enqueue_script('jquery-script', get_stylesheet_directory_uri() . '/inc/webticker/jquery-2.1.4.min.js', array(), '2.1.4', true);
	wp_enqueue_script('jquery-webticker-script', get_stylesheet_directory_uri() . '/inc/webticker/jquery.webticker.min.js', array('jquery'), '', true);
	wp_enqueue_script('jquery-webticker-setting', get_stylesheet_directory_uri() . '/inc/webticker/setting.js', array(), '', true);

}

// Hot News Webticker Function
function hot_news_webticker() {
	$args = array('posts_per_page' => '10');

	$result = '<ul id="webticker">';

	//the loop
	$loop = new WP_Query($args);
	while ($loop -> have_posts()) {
		$loop -> the_post();

		$result .= '<li><a href="' . get_the_permalink() . '"><span class="breaking-news-date">'.human_time_diff( get_the_time('U'), current_time('timestamp') ).' Ago</span>' . get_the_title() . '</a></li>';
	}
	$result .= '</ul>';

	return $result;
}


add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}

// ============ sEmail ================================
if(current_user_can('administrator') || current_user_can('editor')){
	add_filter('post_row_actions', 'add_button_link', 10);
}

function add_button_link($actions) {
	global $post;

	$the_url = wp_get_attachment_url(get_post_thumbnail_id($post -> ID), 'full');

	$data['id'] = $post -> ID;
	$data['title'] = $post -> post_title;
	$data['site_url'] = site_url();
	$data['author_email'] = get_the_author_meta('user_email', $post -> post_author);
	$data['featured_img'] = $the_url;
	$f_img = explode(".", $data['featured_img']);

	$result = array();
	preg_match_all('/<img[^>]+>/i', $post -> post_content, $result);

	if (isset($result)) {
		$img = array();
		foreach ($result as $images) {
			foreach ($images as $img_value) {
				preg_match_all('/(src)=("[^"]*")/i', $img_value, $img[$img_value]);
			}
		}

		foreach ($img as $value) {

			if ($value[2][0] != '"' . $data['featured_img'] . '"') {

				$data['attachment'][] = str_replace('"', '', $value[2][0]);

			}

		}
	}
	$data['iframe'] = 'true';
	$data['width'] = '100%';
	$data['height'] = '100%';

	$qry = http_build_query($data);

	$actions['sEmail_btn'] = '<a href="' . get_stylesheet_directory_uri() . '/inc/form.php?' . $qry . '" rel="prettyPhoto[iframes]">Share via Email</a>';

	return $actions;
}

function my_enqueue($hook) {
	if ('edit.php' != $hook) {
		return;
	}

	wp_enqueue_style('prettyPhoto_style', get_stylesheet_directory_uri() . '/inc/prettyPhoto/css/prettyPhoto.css');
	wp_enqueue_script('jquery_script', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js');
	wp_enqueue_script('prettyPhoto_script', get_stylesheet_directory_uri() . '/inc/prettyPhoto/js/jquery.prettyPhoto.js');
	wp_enqueue_script('prettyPhoto_setting_script', get_stylesheet_directory_uri() . '/inc/prettyPhoto/js/setting.js');
}

add_action('admin_enqueue_scripts', 'my_enqueue');

// =================== Notimail ===================
function post_published_notification($ID, $post) {
	
	if ($post->post_date != $post->post_modified) return;
	
	$to = "tribratanewskediri@gmail.com";

	$author = $post -> post_author;
	/* Post author ID. */
	$name = get_the_author_meta('display_name', $author);
	$email = get_the_author_meta('user_email', $author);
	$title = $post -> post_title;
	$content = $post -> post_content;
	$headers[] = 'From: ' . $name . ' <' . $email . '>';
	$the_attachment = wp_get_attachment_url(get_post_thumbnail_id($post -> ID), 'full');
	 
	wp_mail( $to, $title, $content,  $headers, $the_attachment);
	 
	// Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
	remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );

}
add_action('publish_post', 'post_published_notification', 10, 2);

/***************************************************************
 * First editor row buttons - 4.7 + rearrange
 ***************************************************************/
function tiny_mce_buttons_rearrange( $buttons_array ){
	$mce_buttons = array( 
			'formatselect',		// Dropdown list with block formats to apply to selection.
			'bold',				// Applies the bold format to the current selection.
			'italic',			// Applies the italic format to the current selection.
			'underline',		// Applies the underline format to the current selection.
			'bullist',			// Formats the current selection as a bullet list.
			'numlist',			// Formats the current selection as a numbered list.
			'blockquote',		// Applies block quote format to the current block level element.
			'hr',				// Inserts a horizontal rule into the editor.
			'alignleft',		// Left aligns the current block or image.
			'aligncenter',		// Left aligns the current block or image.
			'alignright',		// Right aligns the current block or image.
			'alignjustify',		// Full aligns the current block or image.
			'link',				// Creates/Edits links within the editor.
			'unlink',			// Removes links from the current selection.
			'wp_more',			// Inserts the <!-- more --> tag.
			'spellchecker',		// ???
			'wp_adv',			// Toggles the second toolbar on/off.
			'dfw' 				// Distraction-free mode on/off.
		); 
	return $mce_buttons;
}


/***************************************************************
 * Second editor row buttons - 4.7 + rearrange
 ***************************************************************/
function tiny_mce_buttons_2_rearrange( $buttons_array ){	
	$mce_buttons_2 = array( 
			'strikethrough',	// Applies strike though format to the current selection.
			'forecolor',		// Applies foreground/text color to selection.
			'pastetext',		// Toggles plain text pasting mode on/off.
			'removeformat',		// Removes the formatting from the current selection.
			'charmap',			// Inserts custom characters into the editor.
			'outdent',			// Outdents the current list item or block element.
			'indent',			// Indents the current list item or block element.
			'undo',				// Undoes the last operation.
			'redo',				// Redoes the last undoed operation.
			'wp_help'			// Opens the help.
		); 
	return $mce_buttons_2;
}

add_filter( 'mce_buttons', 'tiny_mce_buttons_rearrange', 5 );
add_filter( 'mce_buttons_2', 'tiny_mce_buttons_2_rearrange', 5 );

?>