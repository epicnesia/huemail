<?php

require_once("../../../../wp-load.php");

if((isset($_POST['submit']) && !empty($_POST['submit'])) && (isset($_POST['email']) && !empty($_POST['email']))){
		
	$to = explode(",", $_POST['email']);
	
	$subject = $_POST['title'];
	$content    = str_replace('|..|', '"', $_POST['content']);
	
	$headers[] = 'From: ' . $_POST['author_email'] . '  <' . $_POST['author_email'] . '>';
	$headers[] = 'Reply-To: ' . $_POST['author_email'] . '  <' . $_POST['author_email'] . '>';
	
	$the_attachment = array();
	if(isset($_POST['attachment'])){
		foreach ($_POST['attachment'] as $value) {
			$the_attachment[] = $value;
		}
	}
	
	$mail = wp_mail( $to, $subject, $content,  $headers, $the_attachment);
	
	if(!$mail) {
	    echo '<h1>Email tidak terkirim.</h1>';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
	    echo '<h1>Email Terkirim</h1>';
	}
	
	
} else {
	echo '<h1>Email harus diisi</h1>';
	echo '<button onclick="goBack()">Go Back</button>';
	?>
	<script>
		function goBack() {
		    window.history.back();
		}
	</script>
<?php
}


?>