<?php
include "../../../../wp-config.php";

$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Test the connection:
if ($db->connect_errno){
    // Connection Error
    exit("Couldn't connect to the database: ".$db->connect_error);
}

$result = $db->query("SELECT post_content FROM wp_posts WHERE ID='$_GET[id]'");
$content = $result->fetch_object()->post_content;

function clean_attachment_path($site_url, $file)
{
	
	$valid_path = str_replace($site_url, '../../../..', $file);
	
	return $valid_path;
	
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container-fluid">
			<form action="sender.php" method="post">
				<div class="form-group">
					<label for="email">Send To</label>
					<input type="text" name="email" class="form-control" id="email" placeholder="Email" />
					<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
					<input type="hidden" name="author_email" value="<?php echo $_GET['author_email']; ?>" />
					<input type="hidden" name="title" value="<?php echo $_GET['title']; ?>" />
					<input type="hidden" name="content" value="<?php echo str_replace('"', '|..|', strip_tags($content)); ?>" />
					<input type="hidden" name="featured_img" value="<?php echo $_GET['featured_img']; ?>" />
				</div>
				<input type="submit" name="submit" class="btn btn-primary" id="submit" placeholder="Submit" value="Send" />
				
				<h3><?php echo $_GET['title']; ?></h3>
				<?php echo strip_tags($content); ?>
				
				<br /><br />
				<h4>Attachment</h4>
				<?php
				
					if($_GET['featured_img']){
						echo '<input type="hidden" name="attachment[]" value="' . clean_attachment_path($_GET['site_url'], $_GET['featured_img']) . '" />';
						echo '<img src="'.clean_attachment_path($_GET['site_url'], $_GET['featured_img']).'" width="" height="" style="width:250px; height:150px; margin-right: 10px;" />';
					}
					
					if(isset($_GET['attachment'])){
						foreach ($_GET['attachment'] as $value) {
								
							if($value != '"'.$_GET['featured_img'].'"'){
								
								echo '<input type="hidden" name="attachment[]" value=' . clean_attachment_path($_GET['site_url'], $value) . ' />';
				
								echo '<img src="' . clean_attachment_path($_GET['site_url'], $value) . '" width="" height="" style="width:250px; height:150px; margin-right: 10px;" />';
								
							}
								
						}
					}
				?>
			</form>
		</div>
	</body>
</html>
