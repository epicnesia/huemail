$(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto({
			animation_speed: 'fast', /* fast/slow/normal */
			slideshow: false, /* false OR interval time in ms */
			autoplay_slideshow: false, /* true/false */
			show_title: true, /* true/false */
			allow_resize: false, /* Resize the photos bigger than viewport. true/false */
			modal: true, /* If set to true, only the close button will close the window */
			social_tools: false
			});
  });